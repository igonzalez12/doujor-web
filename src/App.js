import React from 'react';
import './App.css';
import Navigation from '../src/components/Navigation/Navigation';

function App() {
    return (<Navigation className="App"/>);
}

export default App;
