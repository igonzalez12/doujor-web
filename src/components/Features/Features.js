import React from 'react';
import './Features.css';
import Card from '../common/Card';
import Header from '../common/Header';
import {Col, Row, Container} from 'react-bootstrap';
import brush from '../../img/001-brush.png';
import startup from '../../img/003-startup.png';
import social from '../../img/005-social-care.png';
import medal from '../../img/006-medal.png';
import download from '../../img/007-download.png';
import friend from '../../img/008-friend.png';

const Features = () => {
    const cardsOne = [
        {
            id: 1,
            title: "Explore the Craft",
            body: "Explore food around you, made by the best people",
            img: brush
        }, {
            id: 2,
            title: "Tailor-Made Connections",
            body: "Our app will recognize best available options out there, just for you! Once you " +
                    "find a quality Chef, we will make sure that the whole process allows for a quali" +
                    "ty experience.",
            img: startup
        }, {
            id: 3,
            title: "Active Support",
            body: "Have any questions, comments or concerns? We got you. We will be providing activ" +
                    "e support for connections being made.",
            img: social
        }
    ];
    const cardsTwo = [
        {
            id: 4,
            title: "Premium Quality",
            body: "Our onboarding process includes quality screening to make sure you have the best" +
                    " experience.",
            img: medal
        }, {
            id: 5,
            title: "Simply Download",
            body: "Easy user sign up and preference-based connections show up in your feed right aw" +
                    "ay.",
            img: download
        }, {
            id: 6,
            title: "User Friendly",
            body: "To ensure a quality experience, we have designed our app to make the connection " +
                    "with your Chef is tailor-made and effortless.",
            img: friend
        }
    ]

    return (
        <div className="features-container" id="features">
            <Header
                title="Features"
                subTitle="The app will include a variety of features to make sure you have the best experience!"/>
            <Container>
                <Row className="justify-content-md-center">
                    {cardsOne.map((card) => <Col key={card.id} className="features-cards" sm={12} md={6} lg={4}><Card title={card.title} body={card.body} img={card.img}/></Col>)}
                </Row>
                <Row className="justify-content-md-center">
                    {cardsTwo.map((card) => <Col key={card.id} className="features-cards" sm={12} md={6} lg={4}><Card title={card.title} body={card.body} img={card.img}/></Col>)}
                </Row>
            </Container>
        </div>
    );
}

export default Features;