import React, {Component} from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import './Navigation.css';
import logo from '../../img/doujorlogo.png';
import Home from '../Home/Home';
import About from '../About/About';
import ComingSoon from '../ComingSoon/ComingSoon';
import HowItWorks from '../HowItWorks/HowItWorks';
import Features from '../Features/Features';
import BecomeChef from '../BecomeChef/BecomeChef';
import Contact from '../Contact/Contact';
import Button from '../common/button';
import Modal from '../common/Modal';

class Navigation extends Component {
    constructor(props) {
        super();

        this.state = {
            showModal: false
        };
    }

    openModal() {
        this.setState({showModal: true});
    }

    closeModal() {
        this.setState({showModal: false});
    }

    render() {
        return (
            <div className="nav-body">
                <Navbar variant="light" expand="lg" bg="light" sticky="top">
                    <img src={logo} alt="logo" className="nav-logo d-inline-block align-top"/>
                    <span className="nav-logo-color">DoJour</span>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                    <Navbar.Collapse className="responsive-navbar-nav justify-content-end">
                        <Nav className="justify-content-end navbar" activeKey="/">
                            <Nav.Item>
                                <Nav.Link href="#home">Home</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link href="#about">About</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link href="#how-it-works">How It Works</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link href="#features">Features</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link href="#become-a-chef">Become A Chef</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link href="#contact">Contact</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Button
                                    text="Get First Access"
                                    onClick={() => this.openModal()}
                                    color="#08A6E4"/>
                            </Nav.Item>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>

                <Modal
                    title="Get First Access"
                    onHide={() => this.closeModal()}
                    show={this.state.showModal}/>
                <Home id="home "/>
                <About id="about"/>
                <ComingSoon id="coming-soon"/>
                <HowItWorks id="how-it-works"/>
                <Features id="features"/>
                <BecomeChef id="become-a-chef"/>
                <Contact id="contact"/>
            </div>
        );
    }
}

export default Navigation;