import React from 'react';
import {Col, Row} from 'react-bootstrap';
import './About.css';
import cook from '../../img/cook.png'
import Header from '../common/Header';

const About = () => {
    const aboutText = "Making your food is an art. We believe in putting a face to their food, as your " +
            "experience with chefs will be unique and more personal than ever.";
    const aboutTextTwo = "This company was founded in 2019 on the basis of two principles:";

    return (
        <div className="about-container" id="about">
            <Header title="About" subTitle={aboutText} subTitleTwo={aboutTextTwo}/>
            <Row>
                <Col xs={12} md={6}>
                    <div>
                        <p className="column-text">1. Give more credit to the people who put so much
                            effort into crafting your food while giving more of a personal experience to
                            customers.</p>
                        <p className="column-text">2. To allow chefs to be as unique as ever, create a
                            platform that will let them create their own "meal of the day" and display their
                            work, as people can see their craft.</p>
                    </div>
                </Col>
                <Col xs={12} md={6}>
                    <img className="image" src={cook} alt="about"/>
                </Col>
            </Row>
        </div>
    )
}

export default About;