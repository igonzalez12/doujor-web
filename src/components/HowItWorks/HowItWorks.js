import React, {Component} from 'react';
import './HowItWorks.css';
import Youtube from 'react-youtube';
import Header from '../common/Header';

class HowItWorks extends Component {
    constructor() {
        super();

        this.state = {
            width: '375',
            height: '390'
        };
    }

    render() {
        const videoId = 'tAaJByZeQrI';

        const opts = {
            width: this.state.width,
            height: this.state.height
        }

        return (
            <div className="how-container" id="how-it-works">
                <div><Header title="How It Works"/></div>
                <Youtube videoId={videoId} SameSite="None" opts={opts}/>
            </div>
        );
    }
}

export default HowItWorks;