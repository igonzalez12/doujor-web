import React, {Component} from 'react';
import './common.css';

class Header extends Component {

    render() {
        return (
            <div>
                <div className="header">
                    <h4 className="header-title">{this.props.title}</h4>
                </div>
                <p className="header-subtitle">
                    {this.props.subTitle}
                </p>
                {/* <p className="header-subtitle">
                    {this.props.subTitleTwo}
                </p> */}
            </div>
        );
    }
}

export default Header;