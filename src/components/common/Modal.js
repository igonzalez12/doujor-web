import React, {Component} from 'react';
import {Modal, Col, Row, FormControl} from 'react-bootstrap';
import Button from '../common/button';
import messagingAPI from '../../api/messagingAPI';

class ModalComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            name: '',
            email: '',
            message: '',
            showMessage: false,
            successMessage: 'Message Sent!'
        }
        this.displayMessage = this
            .displayMessage
            .bind(this);
    }

    onHide() {
        this.setState({show: false});
    }

    resetMessaging() {
        this.setState({name: '', email: '', message: '', showMessage: false});
    }

    displayMessage() {
        this.setState({showMessage: true});
    }

    componentWillReceiveProps(newProps) {
        this.setState({show: newProps.show});
    }

    handleChange(event) {
        const name = event.target.name;
        const value = event.target.value;

        this.setState({[name]: value});
    }

    sendMessage() {
        messagingAPI
            .messaging(this.state.name, this.state.email, this.state.message)
            .then(response => {
                this.displayMessage();

                setTimeout(() => {
                    this.onHide()
                    this.resetMessaging();
                }, 1000);
            })
            .catch(error => {
                this.setState({showMessage: true, successMessage: error});
                this.displayMessage();
            });
    }

    render() {
        return (
            <Modal
                show={this.state.show}
                onHide={() => this.onHide()}
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header closeButton>
                    <Modal.Title>{this.props.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row>
                        <Col xs={12} md={6} lg={6} className="contact-inputs"><FormControl
                            type="text"
                            name="name"
                            placeholder="Name"
                            value={this.state.name}
                            onChange={(event) => this.handleChange(event)}/></Col>
                        <Col xs={12} md={6} lg={6} className="contact-inputs"><FormControl
                            type="text"
                            name="email"
                            placeholder="Email"
                            value={this.state.email}
                            onChange={(event) => this.handleChange(event)}/></Col>
                    </Row>
                    <Row className="contact-textarea">
                        <Col><FormControl
                            as="textarea"
                            rows="3"
                            name="message"
                            placeholder="Message"
                            value={this.state.message}
                            onChange={(event) => this.handleChange(event)}/></Col>
                    </Row>
                    <Row className="justify-content-end">
                        <Col>{this.state.showMessage === true
                                ? <span>{this.state.successMessage}</span>
                                : null}</Col>
                        <Button text="Send Message" onClick={() => this.sendMessage()} color="#08A6E4"/>
                    </Row>
                </Modal.Body>
            </Modal>
        )
    }
}

export default ModalComponent;