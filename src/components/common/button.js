import React, {Component} from 'react';
import './common.css';

export default class Button extends Component {

    render() {
        return (
            <button className="button" style={{"background": `${this.props.color}`}} onClick={this.props.onClick}>
                <a className="link" href={this.props.url}>{this.props.text}</a>
            </button>
        );
    }
}