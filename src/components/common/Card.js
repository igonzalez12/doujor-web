import React, {Component} from 'react';
import {Card} from 'react-bootstrap';
import './common.css';

class CardComponent extends Component {

    render() {
        return (
            <Card className="text-center">
                <Card.Body>
                    <img src={this.props.img} alt="card-icon"/>
                    <Card.Text>{this.props.title}</Card.Text>
                    <p>{this.props.body}</p>
                </Card.Body>
            </Card>
        );
    }
}

export default CardComponent;