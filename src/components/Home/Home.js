import React, {Component} from 'react';
import './Home.css';
import Button from '../common/button';
import Modal from '../common/Modal';

class Home extends Component {
    constructor(props) {
        super();

        this.state = {
            showModal: false
        }
    }

    openModal() {
        this.setState({showModal: true});
    }

    closeModal() {
        this.setState({showModal: false});
    }

    // Redirect to user to instagram
    goToInsta() {
        const instaURL = 'https://www.instagram.com/doujorapp/';
        window.location.href = instaURL;
    }

    render() {
        return (
            <div className="home" id="home">
                <div className="home-text">
                    <h1>Your Own Personal Chef.</h1>
                    <p>We believe that our personal experience with food, should be with the artist
                        that created it.</p>
                    <div className="home-button">
                        <Button text="Get First Access" onClick={() => this.openModal()} color="#08A6E4"/>
                        <Button text="Follow Us" onClick={this.goToInsta}/>
                    </div>
                </div>
                <Modal
                    title="Get First Access"
                    onHide={() => this.closeModal()}
                    show={this.state.showModal}/>
            </div>
        );
    }
}

export default Home;