import React, {Component} from 'react';
import './Contact.css';
import Header from '../common/Header';
import {Row, Col, Container, FormControl} from 'react-bootstrap';
import messagingAPI from '../../api/messagingAPI';
import Button from '../common/button';

class Contact extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            email: '',
            message: '',
            showMessage: false,
            successMessage: 'Message Sent!'
        }
        this.handleChange = this.handleChange.bind(this);
    }

    resetMessaging() {
        this.setState({name: '', email: '', message: '', showMessage: false});
    }

    displayMessage() {
        this.setState({showMessage: true});
    }

    sendMessage() {
        messagingAPI
            .messaging(this.state.name, this.state.email, this.state.message)
            .then(response => {
                this.displayMessage();

                setTimeout(() => {
                    this.resetMessaging();
                }, 2000);
            })
            .catch(error => {
                this.setState({showMessage: true, successMessage: error});
                this.displayMessage();
            });
    }

    handleChange(event) {
        const name = event.target.name;
        const value = event.target.value;

        this.setState({[name]: value});
    }

    render() {
        return (
            <div className="contact-container" id="contact">
                <Header title="Contact Us"/>
                <Container>
                    <Row>
                        <Col xs={12} sm={12} lg={4}>
                            <p>Any Queries?
                                <br/>
                                Feel free to Send us
                                <br/>
                                a message.</p>
                        </Col>
                        <Col className="justify-content-md-center">
                            <Row >
                                <Col xs={12} md={6} lg={6} className="contact-inputs"><FormControl
                                    type="text"
                                    name="name"
                                    placeholder="Name"
                                    value={this.state.name}
                                    onChange={(event) => this.handleChange(event)}/></Col>
                                <Col xs={12} md={6} lg={6} className="contact-inputs"><FormControl
                                    type="text"
                                    name="email"
                                    placeholder="Email"
                                    value={this.state.email}
                                    onChange={(event) => this.handleChange(event)}/></Col>
                            </Row>
                            <Row className="contact-textarea">
                                <Col><FormControl
                                    as="textarea"
                                    rows="3"
                                    name="message"
                                    placeholder="Message"
                                    value={this.state.message}
                                    onChange={(event) => this.handleChange(event)}/></Col>
                            </Row>
                            <Row>
                                <Col>{this.state.showMessage === true
                                        ? <span>{this.state.successMessage}</span>
                                        : null}</Col>
                            </Row>
                            <div className="contact-button">
                                <Button text="Send Message" color="#08A6E4" onClick={() => this.sendMessage()}>Send Message</Button>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default Contact;
