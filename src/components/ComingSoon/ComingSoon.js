import React from 'react';
import './ComingSoon.css';
import googleplay from '../../img/googleplay.png';
import appstore from '../../img/appstore.png';
import Header from '../common/Header';

const ComingSoon = () => {

    return (
        <div className="coming-soon-container" id="coming-soon">
            <Header title="Get The App Soon"/>
            <img src={googleplay} alt="google-play-icon"/>
            <img src={appstore} alt="app-store-icon"/>
            <p className="coming-soon-text">The app will be available November 2019 on Android and iOS platforms.</p>
        </div>
    );
}

export default ComingSoon;