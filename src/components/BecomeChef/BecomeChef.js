import React from 'react';
import Header from '../common/Header';
import './BecomeChef.css';
import {Row, Col} from 'react-bootstrap';
import {FaCheckCircle, FaTimesCircle} from 'react-icons/fa';
// import Button from '../common/button';

const BecomeChef = () => {

    return (
        <div className="become-chef-container" id="become-a-chef">
            <div>
                <Header
                    title="Are you a Chef?"
                    subTitle="Sign up for early access to a platform that can help you build your dream of starting your own business!"/>
            </div>
            <Row className="justify-content-center">
                <Col xs={8} sm={6} md={6} lg={4}>
                    <div className="become-list">
                        <h4 className="become-title">Become A Chef</h4>
                        <p><FaCheckCircle color="green"/>&nbsp;Easy Onboarding Process</p>
                        <p><FaCheckCircle color="green"/>&nbsp;Display Your Work</p>
                        <p><FaCheckCircle color="green"/>&nbsp;Get Paid for Your Awesomeness!</p>
                        <p><FaTimesCircle color="red"/>&nbsp;Negative Attitudes</p>
                        <p><FaTimesCircle color="red"/>&nbsp;Non-Creativity</p>
                    </div>
                </Col>
            </Row>
            {/* <Button text="Get Started" className="become-button" color="#08A6E4">Get Started</Button> */}
        </div>
    );
}

export default BecomeChef;