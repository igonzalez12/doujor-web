const messaging = (name, email, message) => {
    let URL = 'https://api.doujor.com/contact-us';
    let body = new FormData();
    body.append('name', name);
    body.append('email', email);
    body.append('comment', message);

    const sendMessage = fetch(URL, {
            method: 'post',
            body: body
        })
        .then(response => {
        return response;
    })
        .catch(error => {
            return error;
        });
    return sendMessage;
}

export default {
    messaging
}